import React from 'react';
import './App.css';
import { Provider } from 'react-redux';

import AppRouter from './AppRouter';
import configureStore from './configureStore';
import { firestore } from './firebase';
import {
  loadCycleCountFromDB,
  setCycleCounts,
} from './actions/cyclecount';


const store = configureStore();

// Disabling a single load, since the listener will do it.
//store.dispatch(loadCycleCountFromDB());

// Setup a listener - not sure the best place for this.
const listener = firestore.collection('cyclecounts')
  .onSnapshot((querySnapshot) => {
    const cycleCounts = [];
    querySnapshot.forEach((doc) => {
      console.log('snapshot forEach', doc.id);
      cycleCounts.push({ id: doc.id, ...doc.data() });
    });
    console.log('snapshot triggered', cycleCounts.map((cc) => cc.id), cycleCounts);
    store.dispatch(setCycleCounts(cycleCounts));
  });

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <AppRouter />
      </Provider>
    </div>
  );
}

export default App;
