import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import cycleCountReducer from './reducers/cyclecount';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configureStore = () => {
  const store = createStore(
    cycleCountReducer,
    composeEnhancers(applyMiddleware(thunk)));
  return store;
};

export default configureStore;