import { firestore } from '../firebase';
/**
 * The Redux actions for cycle count
 *
 * This will contain the actions to be processed by the reducer and update the DB.
 */

export const addCycleCount = (cycleCount) => ({
   type: 'ADD_CYCLECOUNT',
   cycleCount,
});

export const addCycleCountAndStore = (cycleCount) => {
   return (dispatch) => {
      // TODO - setup default values

      return firestore.collection('cyclecounts')
         .add(cycleCount)
         .then((docRef) => {
            // Disabled since whe have a live listener on the docs via Firestore.
            // dispatch(addCycleCount({
            //    id: docRef.id,
            //    ...cycleCount,
            // }));
         });
   };
};

export const setCycleCounts = (cycleCounts) => ({
   type: 'SET_CYCLECOUNTS',
   cycleCounts,
});
