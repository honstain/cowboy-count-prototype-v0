import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';


const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
}


app.initializeApp(config);
const firestore = app.firestore();

export { firestore };


/**
 * Example queries to validate the configuration.
 */
// const notes = [
//   {title: 'First note'},
//   {title: 'Second note'},
// ];

// notes.map((exp) => {
//   return firestore.collection('notes').add(exp)
//     .then((docRef) => console.log('Document written with id', docRef.id));
// });
