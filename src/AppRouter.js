import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import CycleCountDashboardPage from './components/CycleCountDashboardPage';
import CreateCycleCountPage from './components/CreateCycleCountPage';


const AppRounter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route path="/" component={CycleCountDashboardPage} exact={true}/>
        <Route path="/create" component={CreateCycleCountPage} exact={true}/>
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRounter;