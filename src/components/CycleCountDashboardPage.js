import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';


export const CycleCountDashboardPage = (props) => (
  <div>
    <h3>CYCLE COUNT DASHBOARD</h3>
    {
      props.cycleCounts.map((cc, index) => (
        <p key={index}>{cc.location}, {cc.sku}, {moment(cc.createdAt).format('HH:mm:ss')}</p>
      ))
    }
  </div>
);

const mapStateToProps = (state) => {
  return {
    cycleCounts: state,
  };
};

export default connect(mapStateToProps)(CycleCountDashboardPage);