import React from 'react';


import { NavLink } from 'react-router-dom';

const Header = () => (
  <header>
    <h1>Cowboy Count</h1>
    <h3>Cycle Count Done Quick</h3>
    <NavLink to="/" activeClassName="is-active" exact={true}>Dashboard</NavLink>
    <NavLink to="/create" activeClassName="is-active">Create Cycle Count</NavLink>
  </header>
);

export default Header;