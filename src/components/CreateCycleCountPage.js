import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { addCycleCountAndStore } from '../actions/cyclecount';


export class CreateCycleCountPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      sku: '',
    };
  };
  onLocationChange = (e) => {
    const location = e.target.value;
    this.setState(() => ({ location }));
  }
  onSKUChange = (e) => {
    const sku = e.target.value;
    this.setState(() => ({ sku }));
  }
  onSubmit = (e) => {
    e.preventDefault();
    const cycleCount = {
      location: this.state.location,
      sku: this.state.sku,
      createdAt: moment().valueOf(),
    };
    this.props.addCycleCountAndStore(cycleCount);
    this.props.history.push('/');
  }
  render() {
    return (
      <div>
        <h3>CYCLE COUNT CREATION</h3>
        <form onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="Location"
            value={this.state.location}
            onChange={this.onLocationChange}
          />
          <input
            type="text"
            placeholder="SKU"
            value={this.state.sku}
            onChange={this.onSKUChange}
          />
          <button>Submit</button>
        </form>
      </div>
    );
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCycleCountAndStore: (cycleCount) => dispatch(addCycleCountAndStore(cycleCount)),
  };
};

export default connect(undefined, mapDispatchToProps)(CreateCycleCountPage);