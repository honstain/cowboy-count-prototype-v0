import cycleCountReducer from '../../reducers/cyclecount';
import cycleCounts from '../fixtures/cycleCounts';


test('should set default state', () => {
  const state = cycleCountReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual([]);
});

test('should add cycle count', () => {
  const cycleCount = cycleCounts[0];
  const state = cycleCountReducer([], { type: 'ADD_CYCLECOUNT' , cycleCount });
  expect(state).toEqual([cycleCount]);
});

test('should set cycle counts', () => {
  const state = cycleCountReducer([], { type: 'SET_CYCLECOUNTS' , cycleCounts });
  expect(state).toEqual(cycleCounts);
});
