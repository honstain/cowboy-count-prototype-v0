import React from 'react';
import { shallow } from 'enzyme';
import { CreateCycleCountPage } from '../../components/CreateCycleCountPage';

const LOCATION_01 = 'loc-01';
const SKU_01 = 'sku-01';

test('should render add create cycle count page correctly', () => {
  const wrapper = shallow(<CreateCycleCountPage />);
  expect(wrapper).toMatchSnapshot();
});

test('should update state on a location onChange', () => {
  const wrapper = shallow(<CreateCycleCountPage />);
  wrapper.find('input').at(0).simulate('change', { target: { value: LOCATION_01 }});
  expect(wrapper.state('location')).toEqual(LOCATION_01);
});

test('should update state on a SKU onChange', () => {
  const wrapper = shallow(<CreateCycleCountPage />);
  wrapper.find('input').at(1).simulate('change', { target: { value: SKU_01 }});
  expect(wrapper.state('sku')).toEqual(SKU_01);
});

test('should call addCycleCount prop on valid form submit', () => {
  const addCycleCountAndStoreSpy = jest.fn();
  const history = { push: jest.fn() };
  const wrapper = shallow(<CreateCycleCountPage addCycleCountAndStore={addCycleCountAndStoreSpy} history={history}/>);
  wrapper.find('input').at(0).simulate('change', { target: { value: LOCATION_01 }});
  wrapper.find('input').at(1).simulate('change', { target: { value: SKU_01 }});
  wrapper.find('form').simulate('submit', { preventDefault: () => {} });

  expect(addCycleCountAndStoreSpy).toHaveBeenLastCalledWith({
    location: LOCATION_01,
    sku: SKU_01,
    createdAt: expect.any(Number),
  });
  expect(history.push).toHaveBeenCalledWith('/');
});
