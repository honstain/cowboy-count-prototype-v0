import React from 'react';
import { shallow } from 'enzyme';
import { CycleCountDashboardPage } from '../../components/CycleCountDashboardPage';
import cycleCounts from '../fixtures/cycleCounts';

test('should render empty cycle count dashboard page correctly', () => {
  const wrapper = shallow(<CycleCountDashboardPage cycleCounts={[]}/>);
  expect(wrapper).toMatchSnapshot();
});

test('should render cycle count dashboard page correctly', () => {
  const wrapper = shallow(<CycleCountDashboardPage cycleCounts={cycleCounts}/>);
  expect(wrapper).toMatchSnapshot();
});

