import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  addCycleCount,
  addCycleCountAndStore,
  setCycleCounts,
} from '../../actions/cyclecount';
import { firestore } from '../../firebase';

import cycleCounts from '../fixtures/cycleCounts';cycleCounts


const createMockStore = configureMockStore([thunk]);


beforeEach((done) => {
  const batch = firestore.batch();

  // WARNING - this grabs EVERY doc in the collection and deletes it.
  // TODO - this does not give me a warm fuzzy, but we need a way to clean up the test collection.
  //    The TODO peice is aspirational, in that I hope I find something better.
  firestore.collection('cyclecounts').get().then((docRefs) => {
    docRefs.forEach((val) => {
      batch.delete(val.ref);
    });
  }).then(() => {
    return batch.commit();
  }).then(() => done());;
});

test('addCycleCount generates the correct action object', () => {
  const cycleCount = cycleCounts[0];
  const action = addCycleCount(cycleCount);
  expect(action).toEqual({
    type: 'ADD_CYCLECOUNT',
    cycleCount: cycleCount,
  })
});

test('addCycleCountAndStore calls the DB and dispatches the action object', (done) => {
  const store = createMockStore({});

  const { location, sku, createdAt } = cycleCounts[0];

  store.dispatch(addCycleCountAndStore({ location, sku, createdAt })).then(() => {
    const actions = store.getActions();

    expect(actions).toEqual([]);

    done();
  });
  /**
   * I gutted this since I removed the redux dispatch on create, I already have a listener looking for
   * updates on these docs, it was easier to let redux update via the listener, then resolve the conflict.
   *
   * I will want to look into this further down the road.
   */
  //   expect(actions[0]).toEqual({
  //     type: 'ADD_CYCLECOUNT',
  //     cycleCount: {
  //       id: expect.any(String),
  //       location,
  //       sku,
  //       createdAt,
  //     }
  //   });
  //   return firestore.collection('cyclecounts').doc(`${actions[0].cycleCount.id}`).get();
  // }).then((docRef) => {
  //   expect(docRef.data()).toEqual({ location, sku, createdAt } );
  //   done();
  // });
});

test('setCycleCounts generates the correct action object', () => {
  const action = setCycleCounts(cycleCounts);
  expect(action).toEqual({
    type: 'SET_CYCLECOUNTS',
    cycleCounts: cycleCounts,
  })
});